function offset = getDisparity(distanceToScreen,depthToSimulate,interocularDistance)

% calculate the offset that we need in cm for this disparity depth
deltaD = depthToSimulate - distanceToScreen;
offset = (interocularDistance*deltaD)/(2*(distanceToScreen+deltaD));

end
