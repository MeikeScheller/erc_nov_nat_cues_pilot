 function [data,params] = setUpTaskTwoAFC(params,part)

% general stimulus presentation parameters (This will be needed for all
% tasks using the showStimulus function but may change across tasks)
params.timeLimit = 1.5; % response time limit (in seconds), end if no input for 1 minute
params.waitframes = 4; % how many frames to wait for good timing
params.numFrames = round(params.timeLimit/(params.ifi*params.waitframes)); % how many frames we will show in the time limit
params.times = linspace(0,params.timeLimit,params.numFrames); % the time that each frame corresponds to (in seconds)
% whether or not to check for a response during stimulus presentation, we
% might not want to if only showing for set time (e.g., 2IFC task), or we
% might leave the stimulus on screen until response (or timed out)
    %params.checkResponse = 0;
% whether to show stimulus information on the screen
params.showInfo = 0;

% add some keys we need for this task      % removed up/down arrow keys
params.keyIDs{2,1} = 'One';
params.keyInd(2,1) = KbName('1'); 
params.keyIDs{3,1} = 'Two';
params.keyInd(3,1) = KbName('2'); 
% params.keyIDs{4,1} = 'space';
% params.keyInd(4,1) = KbName('space'); 

% track which stimulus parameter is being toggled
data.toggleIDs = {...
    'All Scroll'
    'Disparity Scroll'
    'Motion Scroll'
    'Sound Scroll'
    'Disparity Present'
    'Motion Present'
    'Sound Present'
    'Dot Density'
    'Background Size'
    'Stimulus Size'
    'Distance To Screen'
    'Dot Diameter'
    };
data.toggle = [1;zeros(length(data.toggleIDs)-1,1)];
%data.toggle = [];

% attached participant info to data structure
data.part = part;

% set up empty vector for collecting rts
data.rt = [];


end


