function [output] = GetThresholds(data)

dat = [data.level', data.resp_cat_vec, data.countVector];
prob_further =  dat(:, 2) ./ dat(:, 3);

% Select the data
x = data.level';
resp_proportion = prob_further; 

% Plot the made up data
fit_fig = figure, scatter(x,resp_proportion)
hold on
ylabel('Proportion "further" responses')
xlabel('Depth in cm')
ylim([0 1]);


%----------------------------------------------------
% Cumulative Gaussian
% Fit for neutral background
SPs = [0 0.1, max(x)+5, 10; % Upper limits for guess (g), lapse (l),bias (u) ,sd(v)
    0, 0.03, mean(x), 1; % Start points for g, l, u ,v
    0, 0, min(x)-5, 0]; % Lower limits for  g, l, u ,v

[coeffsRespProportion2, curveRespProportion2] = ...
    fitPsycheCurveWH(x, resp_proportion, SPs);

% Plot psychometic curves
fig_cumGauss = figure, scatter(x,resp_proportion)
hold on
ylabel('Proportion "further" responses');
xlabel('Depth in cm');
ylim([0 1]);
plot(curveRespProportion2(:,1), curveRespProportion2(:,2), 'k','LineStyle', '--');
legend('Proportion', 'CumGauss fit');

% Getting the bias at 50%:
bias_cumGauss = coeffsRespProportion2.u;
  
% Getting the variance:
threshold_cumGauss  = coeffsRespProportion2.v;

output = [bias_logit, threshold_logit, bias_cumGauss, threshold_cumGauss];
output
