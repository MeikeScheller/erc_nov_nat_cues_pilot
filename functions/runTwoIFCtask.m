 function data = runTwoIFCtask(params,part)
% This functions implements a simple 2AFC task that asks participants to
% judge which of two stimuli is further/closer depending on the cue that is
% shown.

% ----- SET UP TRIALS -----
% Define all trials that will be seen ahead of time (e.g., for method of 
% constant stimuli tasks) or that defines parameters of an adapative 
% procedure (e.g., staircase starting value, step size, temination
% rules, etc.)
% This function defines the data structure, with the participant info
% (the part structure) being attached to it (i.e., define data.part =
% part and define some combination of data.trials, data.staircaseParams,
% etc.)
[data,params] = setUpTaskTwoAFC(params,part); 

%% Specify parameters for 2AFC task
%-------------------------------------------------
% Determine which cues to show 
data.disparityPresent = 1;
data.sizePresent = 0;
data.texturePresent = 0;
data.motionPresent = 0;
data.audioPresent = 0;

% Determine which depth should be used as reference
data.referenceDepth = params.distance + 4;

% how much to change the depth each time
data.depthStep = 0.75; % in cm      
offsetFromRef = data.depthStep/2;

% Stimulus range in cm
data.range = [data.referenceDepth-offsetFromRef  - 3, ...
              data.referenceDepth+offsetFromRef  + 3];

%-------------------------------------------------

% Repetitions per comparison distance per block
repsPerDepth = 3;

% Get possible stimulus levels (in cm)
data.level = [min(data.range):data.depthStep:data.referenceDepth-0.375,...
    data.referenceDepth+0.375:data.depthStep:max(data.range)];   

% Get total amount of trials 
trialsPerBlock = length(data.level)*repsPerDepth*2;

% How many blocks will be run
data.blockNr = 4;

% Create empty arrays for data collection
data.resp = strings(trialsPerBlock,data.blockNr);
data.resp_cat = zeros(trialsPerBlock,data.blockNr);
data.resp_cat_vec = zeros(length(data.level),1);
data.countVector = zeros(length(data.level),1);

 
%% START BLOCK
for block = 1:data.blockNr
    data.block = block;
    
    % Create vector with randomized (balanced) order of depths for whole block
    [data.compDepth] = BalanceTrials(trialsPerBlock,1,data.level);
    [data.refDepth]  = BalanceTrials(trialsPerBlock,1,data.referenceDepth);
    
    % Shows block starting screen for 4 seconds
    block1 = 'Hi! Please report which of the two stimuli is closer.';
    block2 = 'Well done! Block 2 is about to start';
    block3 = 'Fab! Block 3 is about to start';
    block4 = 'Here comes block 4';
    block5 = 'Good job! This is the last block';
    Screen('FillRect', params.window, [0.3 0.3 0.3]);
    
    if block == 1
        DrawFormattedText(params.window, block1, 'center', 'center',params.textColour); 
    elseif block == 2
        DrawFormattedText(params.window, block2, 'center', 'center',params.textColour);
    elseif block == 3
        DrawFormattedText(params.window, block3, 'center', 'center',params.textColour); 
    elseif block == 4
        DrawFormattedText(params.window, block4, 'center', 'center',params.textColour); 
    elseif block == 5
        DrawFormattedText(params.window, block5, 'center', 'center',params.textColour); 
    end
    Screen('Flip', params.window);
    pause(4)
    
    %Quit block if quit key is pressed
    [~,~,keyCode] = KbCheck;
    if keyCode(params.keyInd(1,1))
        ShowCursor;
        return
    else
    end
    
    %% START TRIAL
    for currentTrial = 1:trialsPerBlock
        data.trial = currentTrial;
        
        [data,params] = TwoIFCTrial(block, currentTrial, trialsPerBlock, data, params, part);
        
        %Quit block if quit key is pressed
        [~,~,keyCode] = KbCheck;
        if keyCode(params.keyInd(1,1))
            ShowCursor;
            return
        else
        end
        
        % End experiment after last trial in last block was presented
        if currentTrial == trialsPerBlock  && data.block == data.blockNr
            keyCode(params.keyInd(1,1)) = 1;
        end
    end % of trial
 
    % End experiment after last trial in last block was presented
    if currentTrial == trialsPerBlock  && data.block == data.blockNr
        keyCode(params.keyInd(1,1)) = 1;
    end
    
    
end % of block

data.resp = reshape(data.resp,[],1);
data.resp_cat = reshape(data.resp_cat,[],1);
                % if quit key is hit
    

 
