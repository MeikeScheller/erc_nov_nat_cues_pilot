function [data,params] = TwoIFCTrial(block, currentTrial,trialsPerBlock, data, params, part)

     % Randomize the order in which comp and ref are shown
        trialOrder = randi([0 1]);
 
     % --- FIRST STIMULUS ---
        data.stimCount = 1;

     % ----- Determine which depth should be shown -----
        if trialOrder == 0
           data.currentDepth = data.compDepth(data.trial);
        else 
           data.currentDepth = data.refDepth(data.trial);
        end

        disparityDepth = data.currentDepth;
        sizeDepth = data.currentDepth;
        textureDepth = data.currentDepth;
        motionDepth = data.currentDepth;
        audioDepth = data.currentDepth;
           
     % ----- Process cue information -----
       % Find out which cues to display
        disparity = data.disparityPresent;
        size = data.sizePresent;
        texture = data.texturePresent;  
        motion = data.motionPresent;
        audio = data.audioPresent;
        
          
     % ----- Show ther stimulus -----
       % Show the stimulus
         keyCode = showStimulus(params,disparity,size, texture,motion,audio,...
              disparityDepth, sizeDepth, textureDepth, ...
              motionDepth, audioDepth, part.interocularDistance,[]);
        
       % If quit key was hit, exit the loop
         if keyCode(params.keyInd(1,1))
            ShowCursor;
            return
         else
            while any(keyCode)
                keyCode = KbCheck;
            end
         end
        
         
     % --- Inter-stimulus interval ---
       % Shows blank screen for half the time of each trial
        Screen('FillRect', params.window, [0.3 0.3 0.3]);
        Screen('Flip', params.window);
        pause(params.timeLimit/2)
  
  
        
     % --- SECOND STIMULUS ---
          data.stimCount = 2;

     % ----- Determine which depth should be shown -----
          if trialOrder == 0
              data.currentDepth = data.refDepth(data.trial);
          else 
              data.currentDepth = data.compDepth(data.trial);
          end
          
        disparityDepth = data.currentDepth;
        sizeDepth = data.currentDepth;
        textureDepth = data.currentDepth;
        motionDepth = data.currentDepth;
        audioDepth = data.currentDepth;
           
     % ----- Process cue information -----
       % Find out which cues to display
        disparity = data.disparityPresent;
        size = data.sizePresent;
        texture = data.texturePresent;  
        motion = data.motionPresent;
        audio = data.audioPresent;
            
     % ----- Show the stimulus -----
          keyCode = showStimulus(params,disparity,size, texture,motion,audio,...
              disparityDepth, sizeDepth, textureDepth, ...
              motionDepth,audioDepth, part.interocularDistance,[]);
          
          % If no response key hit show response screen
          if ~any(keyCode(params.keyInd))
              % Show question and instructions
              Screen('FillRect', params.window, [0.3 0.3 0.3]);
              question = 'Which of the two stimuli was further away?';
              optionFirst = 'Press 1 for the first one.';
              optionSecond = 'Press 2 for the second one.';
              DrawFormattedText(params.window, question,...
                  0.30*params.screenXpixels, 0.25*params.screenYpixels,...
                  params.textColour+([0.8 0.8 0.8]));
              DrawFormattedText(params.window, optionFirst ,...
                  0.30*params.screenXpixels, 0.50*params.screenYpixels,...
                  params.textColour+([0.7 0.7 0.7]));
              DrawFormattedText(params.window, optionSecond,...
                  0.30*params.screenXpixels, 0.60*params.screenYpixels,...
                  params.textColour+([0.7 0.7 0.7]));
              Screen('Flip', params.window);
          end
          
          % wait for a key press
           while ~any(keyCode(params.keyInd)) 
               [~,keyCode] = KbWait([],2);
           end
                      
      % ----- PROCESS RESPONSE -----
         % find out what the button press was and code the response
         
         % if quit key was hit, exit the loop
         if keyCode(params.keyInd(1,1))
             ShowCursor;
             return
             % if the 1 key was hit...
         elseif keyCode(params.keyInd(2,1))
             resp = 'first';  %...FIRST stimulus was selected
             if trialOrder == 0
                 resp_cat = 1;   %for comparison
             else
                 resp_cat = 0;   %for reference
             end
             % if 2 key was hit...
         elseif keyCode(params.keyInd(3,1))
             resp = 'second'; %...SECOND stimulus was selected
             if trialOrder == 0
                 resp_cat = 0; %for reference
             else
                 resp_cat = 1; %for comparison
             end
         end
         
         data.trial(currentTrial,block) = currentTrial;
         data.resp(currentTrial,block) = resp;
         data.resp_cat(currentTrial,block) = resp_cat;
         data.resp_cat_vec(data.level==data.compDepth(data.trial)) = ...
            data.resp_cat_vec(data.level==data.compDepth(data.trial)) + resp_cat;
         data.countVector(data.level==data.compDepth(data.trial)) = ...
            data.countVector(data.level==data.compDepth(data.trial)) + 1;




   
        %% ----- Inter trial interval -----
        
        % --- INTER-TRIAL INTERVAL ---
        % Shows blank screen for half the time of each trial
        Screen('FillRect', params.window, [0.3 0.3 0.3]);
        Screen('Flip', params.window);
        pause(params.timeLimit/2)

                      
            if currentTrial == trialsPerBlock  && data.block == data.blockNr
               keyCode(params.keyInd(1,1)) = 1;
            end
                     
  end  
