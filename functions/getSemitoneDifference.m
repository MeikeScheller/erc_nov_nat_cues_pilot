function semitoneDiff = getSemitoneDifference(distanceToScreen,depthToSimulate)
% map the change in depth to a change in semitones

% semitone change per cm depth change
semitonesPerCM = 0.5;

% get the change in distance (in cm)
deltaD = depthToSimulate - distanceToScreen;

% convert to a semitone change
semitoneDiff = semitonesPerCM*deltaD;

end

